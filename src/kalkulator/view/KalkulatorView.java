
package kalkulator.view;

import javax.swing.JOptionPane;
import kalkulator.controller.KalkulatorController;

public class KalkulatorView extends javax.swing.JFrame {
    
    KalkulatorController calcCtrl;
    
    private int currentOperation;
    private float num;
    
    private boolean clearCheck = false;
    private boolean memCheck= false;
    private boolean firstRun = true;
    
    //Constructor
    public KalkulatorView() {
        initComponents();
        this.setLocationRelativeTo(null);
        calcCtrl = new KalkulatorController(this);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        tombolEmpat = new javax.swing.JButton();
        tombolTujuh = new javax.swing.JButton();
        tombolDua = new javax.swing.JButton();
        tombolSatu = new javax.swing.JButton();
        tombolDelapan = new javax.swing.JButton();
        tombolNol = new javax.swing.JButton();
        tombolLima = new javax.swing.JButton();
        tombolEnam = new javax.swing.JButton();
        tombolSembilan = new javax.swing.JButton();
        tombolTiga = new javax.swing.JButton();
        tombolHapus = new javax.swing.JButton();
        tombolTambah = new javax.swing.JButton();
        tombolMasuk = new javax.swing.JButton();
        tombolKeluar = new javax.swing.JButton();
        tombolHasil = new javax.swing.JButton();
        tombolKurang = new javax.swing.JButton();
        tombolKali = new javax.swing.JButton();
        tombolBagi = new javax.swing.JButton();
        tombolClear = new javax.swing.JButton();
        tombolTitik = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        CalcField = new javax.swing.JTextField();
        tombolHapusAngka = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Kalkulator");
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(221, 221, 221));

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));

        tombolEmpat.setBackground(new java.awt.Color(255, 255, 255));
        tombolEmpat.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        tombolEmpat.setText("4");
        tombolEmpat.setBorder(null);
        tombolEmpat.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        tombolEmpat.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                tombolEmpatMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                tombolEmpatMouseExited(evt);
            }
        });
        tombolEmpat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tombolEmpatActionPerformed(evt);
            }
        });

        tombolTujuh.setBackground(new java.awt.Color(255, 255, 255));
        tombolTujuh.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        tombolTujuh.setText("7");
        tombolTujuh.setBorder(null);
        tombolTujuh.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        tombolTujuh.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                tombolTujuhMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                tombolTujuhMouseExited(evt);
            }
        });
        tombolTujuh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tombolTujuhActionPerformed(evt);
            }
        });

        tombolDua.setBackground(new java.awt.Color(255, 255, 255));
        tombolDua.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        tombolDua.setText("2");
        tombolDua.setBorder(null);
        tombolDua.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        tombolDua.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                tombolDuaMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                tombolDuaMouseExited(evt);
            }
        });
        tombolDua.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tombolDuaActionPerformed(evt);
            }
        });

        tombolSatu.setBackground(new java.awt.Color(255, 255, 255));
        tombolSatu.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        tombolSatu.setText("1");
        tombolSatu.setBorder(null);
        tombolSatu.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        tombolSatu.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                tombolSatuMouseExited(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                tombolSatuMouseEntered(evt);
            }
        });
        tombolSatu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tombolSatuActionPerformed(evt);
            }
        });

        tombolDelapan.setBackground(new java.awt.Color(255, 255, 255));
        tombolDelapan.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        tombolDelapan.setText("8");
        tombolDelapan.setBorder(null);
        tombolDelapan.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        tombolDelapan.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                tombolDelapanMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                tombolDelapanMouseExited(evt);
            }
        });
        tombolDelapan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tombolDelapanActionPerformed(evt);
            }
        });

        tombolNol.setBackground(new java.awt.Color(255, 255, 255));
        tombolNol.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        tombolNol.setText("0");
        tombolNol.setBorder(null);
        tombolNol.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        tombolNol.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                tombolNolMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                tombolNolMouseExited(evt);
            }
        });
        tombolNol.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tombolNolActionPerformed(evt);
            }
        });

        tombolLima.setBackground(new java.awt.Color(255, 255, 255));
        tombolLima.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        tombolLima.setText("5");
        tombolLima.setBorder(null);
        tombolLima.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        tombolLima.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                tombolLimaMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                tombolLimaMouseExited(evt);
            }
        });
        tombolLima.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tombolLimaActionPerformed(evt);
            }
        });

        tombolEnam.setBackground(new java.awt.Color(255, 255, 255));
        tombolEnam.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        tombolEnam.setText("6");
        tombolEnam.setBorder(null);
        tombolEnam.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        tombolEnam.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                tombolEnamMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                tombolEnamMouseExited(evt);
            }
        });
        tombolEnam.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tombolEnamActionPerformed(evt);
            }
        });

        tombolSembilan.setBackground(new java.awt.Color(255, 255, 255));
        tombolSembilan.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        tombolSembilan.setText("9");
        tombolSembilan.setBorder(null);
        tombolSembilan.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        tombolSembilan.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                tombolSembilanMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                tombolSembilanMouseExited(evt);
            }
        });
        tombolSembilan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tombolSembilanActionPerformed(evt);
            }
        });

        tombolTiga.setBackground(new java.awt.Color(255, 255, 255));
        tombolTiga.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        tombolTiga.setText("3");
        tombolTiga.setBorder(null);
        tombolTiga.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        tombolTiga.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                tombolTigaMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                tombolTigaMouseExited(evt);
            }
        });
        tombolTiga.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tombolTigaActionPerformed(evt);
            }
        });

        tombolHapus.setBackground(new java.awt.Color(255, 255, 255));
        tombolHapus.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        tombolHapus.setText("Mem.Del ");
        tombolHapus.setBorder(null);
        tombolHapus.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        tombolHapus.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                tombolHapusMouseExited(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                tombolHapusMouseEntered(evt);
            }
        });
        tombolHapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tombolHapusActionPerformed(evt);
            }
        });

        tombolTambah.setBackground(new java.awt.Color(255, 255, 255));
        tombolTambah.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        tombolTambah.setText("+");
        tombolTambah.setBorder(null);
        tombolTambah.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        tombolTambah.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                tombolTambahMouseExited(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                tombolTambahMouseEntered(evt);
            }
        });
        tombolTambah.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tombolTambahActionPerformed(evt);
            }
        });

        tombolMasuk.setBackground(new java.awt.Color(255, 255, 255));
        tombolMasuk.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        tombolMasuk.setText("Mem. In");
        tombolMasuk.setBorder(null);
        tombolMasuk.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        tombolMasuk.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                tombolMasukMouseExited(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                tombolMasukMouseEntered(evt);
            }
        });
        tombolMasuk.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tombolMasukActionPerformed(evt);
            }
        });

        tombolKeluar.setBackground(new java.awt.Color(255, 255, 255));
        tombolKeluar.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        tombolKeluar.setText("Mem. Out");
        tombolKeluar.setBorder(null);
        tombolKeluar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        tombolKeluar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                tombolKeluarMouseExited(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                tombolKeluarMouseEntered(evt);
            }
        });
        tombolKeluar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tombolKeluarActionPerformed(evt);
            }
        });

        tombolHasil.setBackground(new java.awt.Color(255, 255, 255));
        tombolHasil.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        tombolHasil.setText("=");
        tombolHasil.setBorder(null);
        tombolHasil.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        tombolHasil.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                tombolHasilMouseExited(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                tombolHasilMouseEntered(evt);
            }
        });
        tombolHasil.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tombolHasilActionPerformed(evt);
            }
        });

        tombolKurang.setBackground(new java.awt.Color(255, 255, 255));
        tombolKurang.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        tombolKurang.setText("-");
        tombolKurang.setBorder(null);
        tombolKurang.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        tombolKurang.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                tombolKurangMouseExited(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                tombolKurangMouseEntered(evt);
            }
        });
        tombolKurang.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tombolKurangActionPerformed(evt);
            }
        });

        tombolKali.setBackground(new java.awt.Color(255, 255, 255));
        tombolKali.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        tombolKali.setText("x");
        tombolKali.setBorder(null);
        tombolKali.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        tombolKali.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                tombolKaliMouseExited(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                tombolKaliMouseEntered(evt);
            }
        });
        tombolKali.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tombolKaliActionPerformed(evt);
            }
        });

        tombolBagi.setBackground(new java.awt.Color(255, 255, 255));
        tombolBagi.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        tombolBagi.setText("/");
        tombolBagi.setBorder(null);
        tombolBagi.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        tombolBagi.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                tombolBagiMouseExited(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                tombolBagiMouseEntered(evt);
            }
        });
        tombolBagi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tombolBagiActionPerformed(evt);
            }
        });

        tombolClear.setBackground(new java.awt.Color(255, 255, 255));
        tombolClear.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        tombolClear.setText("CE");
        tombolClear.setBorder(null);
        tombolClear.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        tombolClear.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                tombolClearMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                tombolClearMouseExited(evt);
            }
        });
        tombolClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tombolClearActionPerformed(evt);
            }
        });

        tombolTitik.setBackground(new java.awt.Color(255, 255, 255));
        tombolTitik.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        tombolTitik.setText(".");
        tombolTitik.setBorder(null);
        tombolTitik.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        tombolTitik.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                tombolTitikMouseExited(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                tombolTitikMouseEntered(evt);
            }
        });
        tombolTitik.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tombolTitikActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(tombolEmpat, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(tombolLima, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(tombolTujuh, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(tombolTitik, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(tombolDelapan, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(tombolNol, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(tombolEnam, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(tombolMasuk, javax.swing.GroupLayout.DEFAULT_SIZE, 179, Short.MAX_VALUE))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(tombolSembilan, javax.swing.GroupLayout.DEFAULT_SIZE, 106, Short.MAX_VALUE)
                                    .addComponent(tombolClear, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(tombolKeluar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(tombolHasil, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(tombolSatu, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(tombolDua, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(tombolTiga, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(tombolHapus, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(tombolTambah, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(tombolKurang, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addComponent(tombolKali, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(tombolBagi, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(tombolTiga, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(tombolHapus, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(tombolTambah, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(tombolSatu, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(tombolDua, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(tombolEmpat, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tombolLima, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(tombolEnam, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(tombolMasuk, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(tombolKurang, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(tombolDelapan, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tombolTujuh, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(tombolSembilan, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(tombolKeluar, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(tombolKali, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(tombolNol, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(tombolHasil, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(tombolBagi, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(tombolTitik, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(tombolClear, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(25, Short.MAX_VALUE))
        );

        tombolEmpat.setBorder(null);
        tombolEmpat.setContentAreaFilled(false);
        tombolEmpat.setOpaque(true);
        tombolTujuh.setBorder(null);
        tombolTujuh.setContentAreaFilled(false);
        tombolTujuh.setOpaque(true);
        tombolDua.setBorder(null);
        tombolDua.setContentAreaFilled(false);
        tombolDua.setOpaque(true);
        tombolSatu.setBorder(null);
        tombolSatu.setContentAreaFilled(false);
        tombolSatu.setOpaque(true);
        tombolDelapan.setBorder(null);
        tombolDelapan.setContentAreaFilled(false);
        tombolDelapan.setOpaque(true);
        tombolNol.setBorder(null);
        tombolNol.setContentAreaFilled(false);
        tombolNol.setOpaque(true);
        tombolLima.setBorder(null);
        tombolLima.setContentAreaFilled(false);
        tombolLima.setOpaque(true);
        tombolEnam.setBorder(null);
        tombolEnam.setContentAreaFilled(false);
        tombolEnam.setOpaque(true);
        tombolSembilan.setBorder(null);
        tombolSembilan.setContentAreaFilled(false);
        tombolSembilan.setOpaque(true);
        tombolTiga.setBorder(null);
        tombolTiga.setContentAreaFilled(false);
        tombolTiga.setOpaque(true);
        tombolHapus.setBorder(null);
        tombolHapus.setContentAreaFilled(false);
        tombolHapus.setOpaque(true);
        tombolTambah.setBorder(null);
        tombolTambah.setContentAreaFilled(false);
        tombolTambah.setOpaque(true);
        tombolMasuk.setBorder(null);
        tombolMasuk.setContentAreaFilled(false);
        tombolMasuk.setOpaque(true);
        tombolKeluar.setBorder(null);
        tombolKeluar.setContentAreaFilled(false);
        tombolKeluar.setOpaque(true);
        tombolHasil.setBorder(null);
        tombolHasil.setContentAreaFilled(false);
        tombolHasil.setOpaque(true);
        tombolKurang.setBorder(null);
        tombolKurang.setContentAreaFilled(false);
        tombolKurang.setOpaque(true);
        tombolKali.setBorder(null);
        tombolKali.setContentAreaFilled(false);
        tombolKali.setOpaque(true);
        tombolBagi.setBorder(null);
        tombolBagi.setContentAreaFilled(false);
        tombolBagi.setOpaque(true);
        tombolClear.setBorder(null);
        tombolClear.setContentAreaFilled(false);
        tombolClear.setOpaque(true);
        tombolTitik.setBorder(null);
        tombolTitik.setContentAreaFilled(false);
        tombolTitik.setOpaque(true);

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));

        CalcField.setEditable(false);
        CalcField.setBackground(new java.awt.Color(255, 255, 255));
        CalcField.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        CalcField.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        CalcField.setBorder(null);

        tombolHapusAngka.setBackground(new java.awt.Color(255, 255, 255));
        tombolHapusAngka.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        tombolHapusAngka.setText(">");
        tombolHapusAngka.setBorder(null);
        tombolHapusAngka.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        tombolHapusAngka.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                tombolHapusAngkaMouseExited(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                tombolHapusAngkaMouseEntered(evt);
            }
        });
        tombolHapusAngka.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tombolHapusAngkaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addComponent(tombolHapusAngka, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(CalcField)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(CalcField, javax.swing.GroupLayout.DEFAULT_SIZE, 45, Short.MAX_VALUE)
                .addContainerGap())
            .addComponent(tombolHapusAngka, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        tombolSatu.setBorder(null);
        tombolSatu.setContentAreaFilled(false);
        tombolSatu.setOpaque(true);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(23, 23, 23)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tombolSatuMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tombolSatuMouseEntered
        tombolSatu.setBackground(new java.awt.Color(230, 230, 230));
    }//GEN-LAST:event_tombolSatuMouseEntered

    private void tombolSatuMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tombolSatuMouseExited
        tombolSatu.setBackground(new java.awt.Color(255, 255, 255));
    }//GEN-LAST:event_tombolSatuMouseExited

    private void tombolDuaMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tombolDuaMouseEntered
        tombolDua.setBackground(new java.awt.Color(230, 230, 230));
    }//GEN-LAST:event_tombolDuaMouseEntered

    private void tombolDuaMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tombolDuaMouseExited
        tombolDua.setBackground(new java.awt.Color(255, 255, 255));
    }//GEN-LAST:event_tombolDuaMouseExited

    private void tombolTigaMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tombolTigaMouseEntered
        tombolTiga.setBackground(new java.awt.Color(230, 230, 230));
    }//GEN-LAST:event_tombolTigaMouseEntered

    private void tombolTigaMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tombolTigaMouseExited
        tombolTiga.setBackground(new java.awt.Color(255, 255, 255));
    }//GEN-LAST:event_tombolTigaMouseExited

    private void tombolEmpatMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tombolEmpatMouseEntered
        tombolEmpat.setBackground(new java.awt.Color(230, 230, 230));
    }//GEN-LAST:event_tombolEmpatMouseEntered

    private void tombolEmpatMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tombolEmpatMouseExited
        tombolEmpat.setBackground(new java.awt.Color(255, 255, 255));
    }//GEN-LAST:event_tombolEmpatMouseExited

    private void tombolLimaMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tombolLimaMouseEntered
        tombolLima.setBackground(new java.awt.Color(230, 230, 230));
    }//GEN-LAST:event_tombolLimaMouseEntered

    private void tombolLimaMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tombolLimaMouseExited
        tombolLima.setBackground(new java.awt.Color(255, 255, 255));
    }//GEN-LAST:event_tombolLimaMouseExited

    private void tombolEnamMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tombolEnamMouseEntered
        tombolEnam.setBackground(new java.awt.Color(230, 230, 230));
    }//GEN-LAST:event_tombolEnamMouseEntered

    private void tombolEnamMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tombolEnamMouseExited
        tombolEnam.setBackground(new java.awt.Color(255, 255, 255));
    }//GEN-LAST:event_tombolEnamMouseExited

    private void tombolTujuhMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tombolTujuhMouseEntered
        tombolTujuh.setBackground(new java.awt.Color(230, 230, 230));
    }//GEN-LAST:event_tombolTujuhMouseEntered

    private void tombolTujuhMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tombolTujuhMouseExited
        tombolTujuh.setBackground(new java.awt.Color(255, 255, 255));
    }//GEN-LAST:event_tombolTujuhMouseExited

    private void tombolDelapanMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tombolDelapanMouseEntered
        tombolDelapan.setBackground(new java.awt.Color(230, 230, 230));
    }//GEN-LAST:event_tombolDelapanMouseEntered

    private void tombolDelapanMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tombolDelapanMouseExited
        tombolDelapan.setBackground(new java.awt.Color(255, 255, 255));
    }//GEN-LAST:event_tombolDelapanMouseExited

    private void tombolSembilanMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tombolSembilanMouseEntered
        tombolSembilan.setBackground(new java.awt.Color(230, 230, 230));
    }//GEN-LAST:event_tombolSembilanMouseEntered

    private void tombolSembilanMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tombolSembilanMouseExited
        tombolSembilan.setBackground(new java.awt.Color(255, 255, 255));
    }//GEN-LAST:event_tombolSembilanMouseExited

    private void tombolNolMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tombolNolMouseEntered
        tombolNol.setBackground(new java.awt.Color(230, 230, 230));
    }//GEN-LAST:event_tombolNolMouseEntered

    private void tombolNolMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tombolNolMouseExited
        tombolNol.setBackground(new java.awt.Color(255, 255, 255));
    }//GEN-LAST:event_tombolNolMouseExited

    private void tombolHapusMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tombolHapusMouseEntered
        tombolHapus.setBackground(new java.awt.Color(240, 173, 78));
    }//GEN-LAST:event_tombolHapusMouseEntered

    private void tombolHapusMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tombolHapusMouseExited
        tombolHapus.setBackground(new java.awt.Color(255, 255, 255));
    }//GEN-LAST:event_tombolHapusMouseExited

    private void tombolTambahMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tombolTambahMouseEntered
        tombolTambah.setBackground(new java.awt.Color(66,139,202));
    }//GEN-LAST:event_tombolTambahMouseEntered

    private void tombolTambahMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tombolTambahMouseExited
        tombolTambah.setBackground(new java.awt.Color(255, 255, 255));
    }//GEN-LAST:event_tombolTambahMouseExited

    private void tombolMasukMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tombolMasukMouseEntered
        tombolMasuk.setBackground(new java.awt.Color(240, 173, 78));
    }//GEN-LAST:event_tombolMasukMouseEntered

    private void tombolMasukMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tombolMasukMouseExited
        tombolMasuk.setBackground(new java.awt.Color(255, 255, 255));
    }//GEN-LAST:event_tombolMasukMouseExited

    private void tombolKeluarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tombolKeluarMouseEntered
        tombolKeluar.setBackground(new java.awt.Color(240, 173, 78));
    }//GEN-LAST:event_tombolKeluarMouseEntered

    private void tombolKeluarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tombolKeluarMouseExited
        tombolKeluar.setBackground(new java.awt.Color(255, 255, 255));
    }//GEN-LAST:event_tombolKeluarMouseExited

    private void tombolHasilMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tombolHasilMouseEntered
        tombolHasil.setBackground(new java.awt.Color(92, 184, 92));
    }//GEN-LAST:event_tombolHasilMouseEntered

    private void tombolHasilMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tombolHasilMouseExited
        tombolHasil.setBackground(new java.awt.Color(255, 255, 255));
    }//GEN-LAST:event_tombolHasilMouseExited

    private void tombolKurangMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tombolKurangMouseEntered
        tombolKurang.setBackground(new java.awt.Color(66,139,202));
    }//GEN-LAST:event_tombolKurangMouseEntered

    private void tombolKurangMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tombolKurangMouseExited
        tombolKurang.setBackground(new java.awt.Color(255, 255, 255));
    }//GEN-LAST:event_tombolKurangMouseExited

    private void tombolKaliMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tombolKaliMouseEntered
        tombolKali.setBackground(new java.awt.Color(66,139,202));
    }//GEN-LAST:event_tombolKaliMouseEntered

    private void tombolKaliMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tombolKaliMouseExited
        tombolKali.setBackground(new java.awt.Color(255, 255, 255));
    }//GEN-LAST:event_tombolKaliMouseExited

    private void tombolBagiMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tombolBagiMouseEntered
        tombolBagi.setBackground(new java.awt.Color(66,139,202));
    }//GEN-LAST:event_tombolBagiMouseEntered

    private void tombolBagiMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tombolBagiMouseExited
        tombolBagi.setBackground(new java.awt.Color(255, 255, 255));
    }//GEN-LAST:event_tombolBagiMouseExited

    //tombol angka
    private void tombolSatuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tombolSatuActionPerformed
        if(clearCheck){
            this.clrField();
            clearCheck = false;
        }
        CalcField.setText(CalcField.getText() + "1");
        num =this.toNum(CalcField.getText());memCheck=false;
        memCheck=false;
    }//GEN-LAST:event_tombolSatuActionPerformed

    private void tombolDuaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tombolDuaActionPerformed
        if(clearCheck){
            this.clrField();
            clearCheck = false;
        }
        CalcField.setText(CalcField.getText() + "2");
        num =this.toNum(CalcField.getText());
        memCheck=false;
    }//GEN-LAST:event_tombolDuaActionPerformed

    private void tombolTigaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tombolTigaActionPerformed
        if(clearCheck){
            this.clrField();
            clearCheck = false;
        }
        CalcField.setText(CalcField.getText() + "3");
        num =this.toNum(CalcField.getText());
        memCheck=false;
    }//GEN-LAST:event_tombolTigaActionPerformed

    private void tombolEmpatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tombolEmpatActionPerformed
        if(clearCheck){
            this.clrField();
            clearCheck = false;
        }
        CalcField.setText(CalcField.getText() + "4");
        num =this.toNum(CalcField.getText());
        memCheck=false;
    }//GEN-LAST:event_tombolEmpatActionPerformed

    private void tombolLimaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tombolLimaActionPerformed
        if(clearCheck){
            this.clrField();
            clearCheck = false;
        }
        CalcField.setText(CalcField.getText() + "5");
        num =this.toNum(CalcField.getText());
        memCheck=false;
    }//GEN-LAST:event_tombolLimaActionPerformed

    private void tombolEnamActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tombolEnamActionPerformed
        if(clearCheck){
            this.clrField();
            clearCheck = false;
        }
        CalcField.setText(CalcField.getText() + "6");
        num =this.toNum(CalcField.getText());
        memCheck=false;
    }//GEN-LAST:event_tombolEnamActionPerformed

    private void tombolTujuhActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tombolTujuhActionPerformed
        if(clearCheck){
            this.clrField();
            clearCheck = false;
        }
        CalcField.setText(CalcField.getText() + "7");
        num =this.toNum(CalcField.getText());
        memCheck=false;
    }//GEN-LAST:event_tombolTujuhActionPerformed

    private void tombolDelapanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tombolDelapanActionPerformed
        if(clearCheck){
            this.clrField();
            clearCheck = false;
        }
        CalcField.setText(CalcField.getText() + "8");
        num =this.toNum(CalcField.getText());
        memCheck=false;
    }//GEN-LAST:event_tombolDelapanActionPerformed

    private void tombolSembilanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tombolSembilanActionPerformed
        if(clearCheck){
            this.clrField();
            clearCheck = false;
        }
        CalcField.setText(CalcField.getText() + "9");
        num = this.toNum(CalcField.getText());
        memCheck=false;
    }//GEN-LAST:event_tombolSembilanActionPerformed

    private void tombolNolActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tombolNolActionPerformed
        if(clearCheck){
            this.clrField();
            clearCheck = false;
        }
        CalcField.setText(CalcField.getText() + "0");
        num =this.toNum(CalcField.getText());
        memCheck=false;
    }//GEN-LAST:event_tombolNolActionPerformed

    //======================================================================
    private void tombolClearMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tombolClearMouseExited
        tombolClear.setBackground(new java.awt.Color(255, 255, 255));
    }//GEN-LAST:event_tombolClearMouseExited

    private void tombolClearMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tombolClearMouseEntered
        tombolClear.setBackground(new java.awt.Color(217, 83, 79));
    }//GEN-LAST:event_tombolClearMouseEntered

    //tombol operasi clear
    private void tombolClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tombolClearActionPerformed
        this.clc();
    }//GEN-LAST:event_tombolClearActionPerformed

    //tombol operasi matematika
    private void tombolTambahActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tombolTambahActionPerformed
        this.operate(1);
    }//GEN-LAST:event_tombolTambahActionPerformed

    private void tombolHasilActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tombolHasilActionPerformed
        this.submit(true);
    }//GEN-LAST:event_tombolHasilActionPerformed

    private void tombolKurangActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tombolKurangActionPerformed
        String tmp = CalcField.getText();
        if(tmp.equals("")){
            CalcField.setText("-");
            clearCheck=false;
        }else if(tmp.equals("0.")){
            CalcField.setText("-0.");
            clearCheck=false;
        }else if(tmp.equals("-0.")){
            CalcField.setText("0.");
            clearCheck=false;
        }else if(tmp.equals("0")||num==0){
            CalcField.setText("-");
            clearCheck=false;
        }else if(tmp.equals("-")){
            this.clrField();
            clearCheck=false;
        }else{
        this.operate(2);
        }
        
    }//GEN-LAST:event_tombolKurangActionPerformed

    private void tombolKaliActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tombolKaliActionPerformed
        this.operate(3);
    }//GEN-LAST:event_tombolKaliActionPerformed

    private void tombolBagiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tombolBagiActionPerformed
        this.operate(4);
    }//GEN-LAST:event_tombolBagiActionPerformed

    //tombol operasi memori
    private void tombolHapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tombolHapusActionPerformed
        try{
            calcCtrl.delMem();
        }catch(Exception e){
        JOptionPane.showMessageDialog(null, "operasi gagal");
        }
    }//GEN-LAST:event_tombolHapusActionPerformed

    private void tombolKeluarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tombolKeluarActionPerformed
        try{
            calcCtrl.memOut();
        }catch(Exception e){
        JOptionPane.showMessageDialog(null, "operasi gagal");
        }
    }//GEN-LAST:event_tombolKeluarActionPerformed

    private void tombolMasukActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tombolMasukActionPerformed
         if(memCheck && !CalcField.getText().equals("")){
        calcCtrl.memIn();
        JOptionPane.showMessageDialog(null, "Tersimpan Ke Memori");
        }else{
         JOptionPane.showMessageDialog(null, "Gagal Tersimpan Ke Memori");
         }
    }//GEN-LAST:event_tombolMasukActionPerformed

//===================================================
    private void tombolTitikMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tombolTitikMouseEntered
        tombolTitik.setBackground(new java.awt.Color(230, 230, 230));
    }//GEN-LAST:event_tombolTitikMouseEntered

    private void tombolTitikMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tombolTitikMouseExited
        tombolTitik.setBackground(new java.awt.Color(255, 255, 255));
    }//GEN-LAST:event_tombolTitikMouseExited

    private void tombolTitikActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tombolTitikActionPerformed
        if(clearCheck){
            this.clrField();
            clearCheck = false;
        }
        String tmp="";
        tmp=CalcField.getText();
        switch(tmp){
            case "":
                CalcField.setText("0.");
                break;
            case "-":
                CalcField.setText("-0.");
                break;
            default:
                if (!tmp.contains(".")) {
                    CalcField.setText(CalcField.getText() + ".");
                }
                break;
        }
        
        num =this.toNum(CalcField.getText());
        memCheck=false;
    }//GEN-LAST:event_tombolTitikActionPerformed

    //operasi tombol hapus angka paling belakang
    private void tombolHapusAngkaMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tombolHapusAngkaMouseExited
        tombolHapusAngka.setBackground(new java.awt.Color(255, 255, 255));
    }//GEN-LAST:event_tombolHapusAngkaMouseExited

    private void tombolHapusAngkaMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tombolHapusAngkaMouseEntered
        tombolHapusAngka.setBackground(new java.awt.Color(217, 83, 79));
    }//GEN-LAST:event_tombolHapusAngkaMouseEntered

    private void tombolHapusAngkaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tombolHapusAngkaActionPerformed
        if(CalcField.getText().equals("0.")||CalcField.getText().equals("-0.")){
            CalcField.setText("");
            num = 0;
        }else if(!CalcField.getText().equals("")){
        String tmp="";
        tmp = CalcField.getText();
        tmp=tmp.substring(0, tmp.length()-1);
        CalcField.setText(tmp);
        }
        
        if(CalcField.getText().equals("")){
        num = 0;
        }else{
        num =this.toNum(CalcField.getText());
        }
        memCheck=false;
    }//GEN-LAST:event_tombolHapusAngkaActionPerformed
    //=================================================
    
    //set angka pada display
    public void setCalcField(float a){
        CalcField.setText(""+a);
    }
    
    //ubah string menjadi float (angka)
    public float toNum(String a){
        float res=0;
        try{
        res = Float.parseFloat(a);
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "Operasi gagal");
            this.clc();
        }
        return res;
    }
    
    //operasi sama dengan
    public void submit(boolean a){
        if(firstRun){
        calcCtrl.setnum(this.toNum(CalcField.getText()));
        currentOperation=9;
        }
        switch(currentOperation){
            case 1:
                calcCtrl.tambah(num);
                currentOperation=9;
                break;
            case 2:
                calcCtrl.kurang(num);
                currentOperation=9;
                break;
            case 3:
                calcCtrl.kali(num);
                currentOperation=9;
                break;
            case 4:
                calcCtrl.bagi(num);
                currentOperation=9;
                break;
        }
        this.setCalcField(calcCtrl.getCurNum());
        firstRun=true;
        clearCheck=true;
        memCheck=true;
    }
    
    //operasi matematika
    public void operate(int a){
        if(CalcField.getText().equals("-") || CalcField.getText().equals("")){
        }else{
        if(firstRun){
            calcCtrl.setnum(this.toNum(CalcField.getText()));
            currentOperation=a;
        }else{
            this.submit(true);
            switch(a){
                case 1:
                    currentOperation=1;
                    break;
                case 2:
                    currentOperation=2;
                    break;
                case 3:
                    currentOperation=3;
                    break;
                case 4:
                    currentOperation=4;
                    break;
            }
        }
        num=0;
        firstRun=false;
        clearCheck=true;   
        }
    }
    
    //clear display tanpa menghapus angka
    private void clrField() {CalcField.setText("");}
   
    //clear semua
    public void clc(){
        this.clrField();
        calcCtrl.clr();
        firstRun=true;
    }
    
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(KalkulatorView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(KalkulatorView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(KalkulatorView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(KalkulatorView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new KalkulatorView().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField CalcField;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JButton tombolBagi;
    private javax.swing.JButton tombolClear;
    private javax.swing.JButton tombolDelapan;
    private javax.swing.JButton tombolDua;
    private javax.swing.JButton tombolEmpat;
    private javax.swing.JButton tombolEnam;
    private javax.swing.JButton tombolHapus;
    private javax.swing.JButton tombolHapusAngka;
    private javax.swing.JButton tombolHasil;
    private javax.swing.JButton tombolKali;
    private javax.swing.JButton tombolKeluar;
    private javax.swing.JButton tombolKurang;
    private javax.swing.JButton tombolLima;
    private javax.swing.JButton tombolMasuk;
    private javax.swing.JButton tombolNol;
    private javax.swing.JButton tombolSatu;
    private javax.swing.JButton tombolSembilan;
    private javax.swing.JButton tombolTambah;
    private javax.swing.JButton tombolTiga;
    private javax.swing.JButton tombolTitik;
    private javax.swing.JButton tombolTujuh;
    // End of variables declaration//GEN-END:variables

}
