
package kalkulator.controller;

import javax.swing.JOptionPane;
import kalkulator.model.KalkulatorModel;
import kalkulator.view.KalkulatorView;

public class KalkulatorController {
    
    float curNumber;
    KalkulatorView calcView;
    KalkulatorModel calcModel= new KalkulatorModel();
    
    //constructor
    public KalkulatorController(KalkulatorView a) {
        curNumber = 0;
        calcView = a;
    }
    
    //clear angka sekarang
    public void clr(){curNumber=0;}
    
    //set angka sekarang
    public void setnum(float a){curNumber=a;}
    
    //operasi pada Controller
    public void tambah(float a){
        calcModel.setTambahHasil(curNumber, a);
        curNumber = calcModel.getHasil();
        calcView.setCalcField(curNumber);
    }
    
    public void kurang(float a){
        calcModel.setKurangHasil(curNumber, a);
        curNumber = calcModel.getHasil();
        calcView.setCalcField(curNumber);
    }
    
    public void kali(float a){    
        calcModel.setKaliHasil(curNumber, a);
        curNumber = calcModel.getHasil();
        calcView.setCalcField(curNumber);
    }
    
    public void bagi(float a){
        if(a==0){
            JOptionPane.showMessageDialog(null, "Pembagian dengan 0 tidak valid");
            calcView.clc();
        }else{
            calcModel.setBagiHasil(curNumber, a);
            curNumber = calcModel.getHasil();
            calcView.setCalcField(curNumber);
        }
    }
    //=========================================================================
    
    //operasi memori
    public void delMem(){calcModel.del();}
    public void memIn(){calcModel.save(curNumber);}
    public void memOut(){calcView.setCalcField(calcModel.get());}
    //=========================================================================
    
    //ambil angka controller
    public float getCurNum(){return curNumber;}
}


