
package kalkulator.model;

public class KalkulatorModel {
    
    private float hasil;
    private float hasilSV;
    
    public KalkulatorModel(){}
    
    //operasi Matematika
    public void setTambahHasil(float bil1, float bil2){hasil = bil1 + bil2;}
    public void setKurangHasil(float bil1, float bil2){hasil = bil1 - bil2;}
    public void setKaliHasil(float bil1, float bil2){hasil = bil1 * bil2;}
    public void setBagiHasil(float bil1, float bil2){
        try{
            hasil = bil1 / bil2;
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    //=======================================
    
    //ambil hasil
    public float getHasil(){return hasil;}
    //save memori
    public void save(){hasilSV = hasil;}
    public void save(float a){hasilSV = a;}
    //delete memori
    public void del(){hasilSV = 0;}
    //ambil memori
    public float get(){return hasilSV;}
}
